import pystache
import json

slides = json.load(open('slides.json'))
open('index.html', 'w').write(
    pystache.render(
        open('slides.mustache').read(), slides))
print('successfully wrote index.html')
open('notes.html', 'w').write(
    pystache.render(
        open('notes.mustache').read(), slides))
print('successfully wrote notes.html')
