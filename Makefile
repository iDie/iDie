all: index.html notes.html

index.html: makeslides.py slides.mustache slides.json
	./makeslides.sh

notes.html: makeslides.py notes.mustache slides.json
	./makeslides.sh

slides.json: csv2json.py slides.csv slides/*.rst
	./csv2json.sh
	git add slides

server: all
	python3 -m http.server
