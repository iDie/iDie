### Slides

Here there's a
[RestructuredText](https://docutils.sourceforge.io/docs/user/rst/quickref.html)
`.rst` file for each slide defined at `slides.csv`.

Originally, these files are automatically generated from `slides.csv` by `csv2json.py`
(containing the `description` column as initial content),
but once the are there, they can be edited manually

Coming soon: iDie specific extensions to markdown syntax.
